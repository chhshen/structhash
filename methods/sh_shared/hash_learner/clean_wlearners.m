
function wlearners=clean_wlearners(wlearners)


bit_num=size(wlearners,1);

% clean wl, to save space when saving the model
for b_idx=1:bit_num
    one_wl=wlearners(b_idx,:);
    if iscell(one_wl)
        one_wl=one_wl{1};
        if isfield(one_wl, 'label_data')
            one_wl.label_data=[];
        end
        if isfield(one_wl, 'hfeat')
            one_wl.hfeat=[];
        end
        wlearners(b_idx,:)={one_wl};
    end
end
