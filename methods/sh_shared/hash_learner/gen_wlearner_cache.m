


function cache_info=gen_wlearner_cache(train_info, train_data, cache_info)


cache_info.wlearner_cache=[];

if train_info.do_wl_train_spectral

   KK=cache_info.cached_feat;
   RM = KK'*KK; 
   
   if rcond(RM) <1e-4
       tmp_num=size(RM, 1);
       RM=RM+diag(1e-4*rand(tmp_num, 1));
   end
   
   cache_info.RM=RM;
end


end



