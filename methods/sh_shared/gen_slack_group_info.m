

function slack_group_info=gen_slack_group_info(slack_idxes)

error('should not come here!');

[tmp_vs, ~, slack_group_idxes]=unique(slack_idxes);

slack_group_info.slack_group_num=length(tmp_vs);
slack_group_info.slack_group_idxes=slack_group_idxes;

end

function slack_group_info=old_gen_slack_group_info(slack_idxes)



pair_num=length(slack_idxes);
slack_idxes_vs=unique(slack_idxes);
slack_num=length(slack_idxes_vs);


slack_group_idxes=zeros(pair_num,1);

if slack_num==pair_num
    c_group_poses=(1:pair_num)';
    c_group_type=3;
    slack_group_idxes(:,1)=1:pair_num;
else
    
    c_group_type=1;
    
end

if c_group_type==1
    c_group_poses=cell(slack_num,1);
    for t_idx=1:length(slack_idxes_vs)
        slack_idx_v=slack_idxes_vs(t_idx);
        pos_sel=(slack_idxes==slack_idx_v);
        poses=find(pos_sel>0);
       
        c_group_poses{t_idx}=poses';
        slack_group_idxes(pos_sel)=t_idx;
    end
end

c_group_poses_cell=c_group_poses;
if ~iscell(c_group_poses_cell)
    c_group_poses_cell=num2cell(c_group_poses_cell,2);
end

slack_group_info.slack_group_poses_mat=c_group_poses;
slack_group_info.slack_group_poses=c_group_poses_cell;
slack_group_info.slack_group_type=c_group_type;
slack_group_info.slack_group_num=slack_num;
slack_group_info.slack_group_idxes=slack_group_idxes;

end