

function gen_result=gen_relation_map(gen_param)
        
        
        gen_type=gen_param.gen_type;
        
        
        gen_result=[];
        
        if strcmp(gen_type, 'triplet')
                        
            [relation_map relation_inds_es]=gen_relation_triplet(gen_param);
            
            gen_result.relation_map=relation_map;
            gen_result.relation_inds_es=relation_inds_es;
        end
        
        if strcmp(gen_type, 'duplet')
            
            relation_info=gen_param.relation_info;
            max_relation_num_per_e=gen_param.max_relation_num_per_e;
            
            [relation_map relevant_sel relation_inds_es]=gen_relation_map_duplet(relation_info, max_relation_num_per_e);
            
            gen_result.relation_map_duplet=relation_map;
            gen_result.relevant_sel_duplet=relevant_sel;
            gen_result.relation_inds_es_duplet=relation_inds_es;
        end
        
        assert(isa(relation_map, 'uint32'));
        
end








function [relation_map relation_inds_es]=gen_relation_triplet(gen_param)

relation_info=gen_param.relation_info;
max_relation_num_per_e=gen_param.max_relation_num_per_e;


sel_e_idxes=1:length(relation_info.relevant_infos);
if isfield(gen_param, 'sel_e_idxes') && ~isempty(gen_param.sel_e_idxes)
    sel_e_idxes=gen_param.sel_e_idxes;
end

e_num=length(sel_e_idxes);


relation_inds_es=cell(e_num,1);
relation_map=cell(e_num,1);

relation_ind_counter=0;

if e_num>1
    fprintf('gen_relation_map, e_num:%d \n', e_num);
end


for e_idx_idx=1:e_num
    
    e_idx=sel_e_idxes(e_idx_idx);
    
    [one_relation_map]=do_gen_relation_map_one(relation_info, e_idx);
    
    one_r_num=size(one_relation_map,1);
    
    if one_r_num==0
        
        relation_map{e_idx_idx}=one_relation_map;      
        relation_inds_es{e_idx_idx}=uint32([]);
        continue;
    end
    
    
    do_sample=one_r_num>max_relation_num_per_e;
    if do_sample
        onsel_e_idxes=randsample(one_pair_num, max_relation_num_per_e);
        one_relation_map=one_relation_map(onsel_e_idxes,:);
        one_r_num=size(one_relation_map,1);
    end
                
    
    one_relation_inds=relation_ind_counter+1:relation_ind_counter+one_r_num;
    one_relation_inds=one_relation_inds';
    relation_ind_counter=relation_ind_counter+one_r_num;
       
    
    relation_map{e_idx_idx}=one_relation_map;      
    relation_inds_es{e_idx_idx}=uint32(one_relation_inds);
    
end


relation_map=cell2mat(relation_map);

assert(relation_ind_counter<2^32);

end









function [one_relation_map]=do_gen_relation_map_one(relation_info, e_idx)

    

    
    p_top_knn_inds=relation_info.relevant_infos{e_idx};
    n_top_knn_inds=relation_info.irrelevant_infos{e_idx};
    
    top_knn_pos_num=length(p_top_knn_inds);
    top_knn_neg_num=length(n_top_knn_inds);
            
    one_relation_map=uint32([]);
    if (top_knn_pos_num+top_knn_neg_num)==0
        return;
    end
        
            

        n_one_map_col=repmat(n_top_knn_inds,top_knn_pos_num, 1);
        n_one_map_col=n_one_map_col(:);
        
        p_one_map_col=repmat(p_top_knn_inds',top_knn_neg_num, 1);
        p_one_map_col=p_one_map_col(:);

        one_relation_map=zeros(length(n_one_map_col),3,'uint32');
        one_relation_map(:,2)=n_one_map_col;
        one_relation_map(:,3)=p_one_map_col;
        one_relation_map(:,1)=e_idx;
       
        one_relation_map=uint32(one_relation_map);
    
    
end




function relation_map=gen_init_relation_map(relation_info)

e_num=length(relation_info.relevant_infos);
relation_map=zeros(e_num,3);

for e_idx=1:e_num

    p_top_knn_inds=relation_info.relevant_infos{e_idx};
    n_top_knn_inds=relation_info.irrelevant_infos{e_idx};
        
    relation_map(e_idx,2)=n_top_knn_inds(1);
    relation_map(e_idx,3)=p_top_knn_inds(1);
    relation_map(e_idx,1)=e_idx;
    
end

relation_map=uint32(relation_map);

end















function [relation_map relevant_sel relation_inds_es]=gen_relation_map_duplet(relation_info, max_relation_num_per_e)



relation_ind_counter=0;
e_num=length(relation_info.relevant_infos);
relation_inds_es=cell(e_num,1);

relation_map=cell(e_num,1);
relevant_sel=cell(e_num,1);


fprintf('gen_relation_map_duplet: e_num:%d \n', e_num);

for e_idx=1:e_num
         
    
    
    p_top_knn_inds=relation_info.relevant_infos{e_idx}';
    n_top_knn_inds=relation_info.irrelevant_infos{e_idx}';
       
   
    top_knn_pos_num=length(p_top_knn_inds);
    top_knn_neg_num=length(n_top_knn_inds);
    one_pair_num=top_knn_pos_num+top_knn_neg_num;
    
    if one_pair_num==0
        relation_map{e_idx}=uint32([]);
        relevant_sel{e_idx}=logical([]);

        
        continue;
    end
        

    one_relation_map_col=cat(2, p_top_knn_inds, n_top_knn_inds);
    one_relevant_sel=false(size(one_relation_map_col));
    one_relevant_sel(:, 1:top_knn_pos_num)=true;
    
    one_relation_map_col=uint32(one_relation_map_col)';
    one_relevant_sel=one_relevant_sel';
      
    
    one_relation_map=zeros(length(one_relation_map_col), 2, 'uint32');
    one_relation_map(:, 2)=one_relation_map_col;
    one_relation_map(:, 1)=uint32(e_idx);
            
    
    one_r_num=size(one_relation_map,1);
    do_sample=one_r_num>max_relation_num_per_e;
    if do_sample
        onsel_e_idxes=randsample(one_pair_num, max_relation_num_per_e);
        one_relation_map=one_relation_map(onsel_e_idxes,:);
        one_relevant_sel=one_relevant_sel(onsel_e_idxes,:);
        one_r_num=size(one_relation_map,1);
    end
            
    one_relation_inds=relation_ind_counter+1:relation_ind_counter+one_r_num;
    one_relation_inds=one_relation_inds';
    relation_ind_counter=relation_ind_counter+one_r_num;
  
    
    relation_inds_es{e_idx}=uint32(one_relation_inds);
         
        
    relation_map{e_idx}=one_relation_map;
    relevant_sel{e_idx}=one_relevant_sel;
   
end


relation_map=cell2mat(relation_map);
relevant_sel=cell2mat(relevant_sel);


assert(relation_ind_counter<2^32);

end



