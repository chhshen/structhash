function duplet_weights=transfer_triplet_weights(triplet_weights, wlearner_cache_duplet)

                
        triple_r_inds_duplets=wlearner_cache_duplet.triple_r_inds_duplets;

        d_num=length(triple_r_inds_duplets);
        duplet_weights=zeros(d_num,1);

        for d_idx=1:d_num
            duplet_weights(d_idx)=sum(triplet_weights(triple_r_inds_duplets{d_idx}));
        end
        
end