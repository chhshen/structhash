

function infer_result=my_infer_sndcg(train_info, cache_info, infer_info)


    hfeat=cache_info.hfeat;
    model_w=infer_info.model_w;
    rescale_type=infer_info.rescale_type;
    e_idx=infer_info.e_idx;
    one_pair_constant=infer_info.one_pair_constant;

    e_num=cache_info.e_num;
        
    relation_info=cache_info.relation_info;
    relevant_infos=relation_info.relevant_infos;
    irrelevant_infos=relation_info.irrelevant_infos;
    
    pos_e_idxes=relevant_infos{e_idx};
    neg_e_idxes=irrelevant_infos{e_idx};
        
   
    
    ep_hfeat=hfeat(pos_e_idxes,:);
    en_hfeat=hfeat(neg_e_idxes,:);

    pos_num=length(pos_e_idxes);
    neg_num=length(neg_e_idxes);
    e_hfeat_tmp=hfeat(e_idx,:);
    e_hfeat=repmat(e_hfeat_tmp, pos_num,1);
    pos_dist=calc_hfeat_dist(e_hfeat,ep_hfeat)*model_w;
    e_hfeat=repmat(e_hfeat_tmp, neg_num,1);
    neg_dist=calc_hfeat_dist(e_hfeat,en_hfeat)*model_w;
    nn_num=pos_num+neg_num;


    ndcg_discount_values=1./log2(2+(1:neg_num));
    tmp_label_loss=1-ndcg_discount_values';
    
    [sort_neg_dist, neg_sort_idxes]=sort(neg_dist);
        
  
        
    tmp_one_pos_constant=one_pair_constant.*pos_num;
    
    pos_scores_cumsum_mat=bsxfun(@times, (pos_dist.*tmp_one_pos_constant), (1:neg_num));
    
    neg_sort_score_cumsum=cumsum(sort_neg_dist).*tmp_one_pos_constant;
    label_loss_neg_sort_score_cumsum=tmp_label_loss-neg_sort_score_cumsum;
    score_mat=bsxfun(@plus, pos_scores_cumsum_mat, label_loss_neg_sort_score_cumsum');
    

    
    
    
    
    [max_scores_pos, max_score_idxes]=max(score_mat, [], 2);
    valid_pos_sel=max_scores_pos>0;

	max_score_idxes_org=max_score_idxes;
    
    non_valid_pos_sel=~valid_pos_sel;
    max_score_idxes(non_valid_pos_sel)=0;
    
    [tmp_sort_inert_idxes, pos_sort_idxes]=sort(max_score_idxes);
    tmp_pos_insert_idxes=tmp_sort_inert_idxes+(1:pos_num)';
    pos_insert_idxes(pos_sort_idxes)=tmp_pos_insert_idxes;
    
    tmp_position_flags_pos_insert=false(nn_num,1);
    tmp_position_flags_pos_insert(pos_insert_idxes)=true;
    neg_insert_idxes=find(~tmp_position_flags_pos_insert);
    
    assert(length(neg_insert_idxes)==neg_num);
    
    
    rank_vs=zeros(e_num,1);
    rank_vs(pos_e_idxes)=pos_insert_idxes;
    rank_vs(neg_e_idxes(neg_sort_idxes))=neg_insert_idxes;
                

    tmp_one_lable_loss=tmp_label_loss(max_score_idxes_org);
    tmp_one_lable_loss(non_valid_pos_sel)=0;
    one_label_loss=mean(tmp_one_lable_loss);
    
    
    
    
    one_full_relation_map=cache_info.relation_map_es{e_idx};
    
       % col3 is the similar items...
        tmp_sel=rank_vs(one_full_relation_map(:,2))<rank_vs(one_full_relation_map(:,3));
        one_sel_relation_idxes_sub=find(tmp_sel);
        one_relation_map=one_full_relation_map(tmp_sel,:);
        one_r_num=length(one_sel_relation_idxes_sub);
               
        
        if one_r_num>0
            
        
            
            one_new_pair_feat_rank=do_calc_pair_feat(hfeat, one_relation_map);
            sel_scores_org=one_new_pair_feat_rank*model_w;
            sel_scores_org_sum=sum(sel_scores_org).*one_pair_constant;    
            
                        
            try
                assert(one_label_loss>0);
            catch
                dbstack;
                keyboard;
            end
            
            
            if rescale_type==1
                error('not support!');
            end

            if rescale_type==2
                max_loss=one_label_loss-sel_scores_org_sum;
            end
            
                            
        else
               
       
                                    
            try
                assert(all(non_valid_pos_sel));
                assert(max(neg_insert_idxes)>pos_num);
            catch
                dbstack;
                keyboard;
            end
            
            max_loss=0;
		one_new_pair_feat_rank=[];
            
        end
        
        infer_result=[];
        infer_result.one_label_loss=one_label_loss;
        infer_result.one_sel_relation_idxes_sub=one_sel_relation_idxes_sub;
        infer_result.one_new_pair_feat_rank=one_new_pair_feat_rank;
        infer_result.max_loss=max_loss;
        
end

