function pair_feat=do_calc_pair_feat(hfeat, relation_map, relevant_sel)
       

    if size(relation_map,2)==3
        pair_feat=do_calc_pair_feat_triplet(hfeat, relation_map);
    end

    if size(relation_map,2)==2
        pair_feat=do_calc_pair_feat_duplet(hfeat, relation_map, relevant_sel);
    end
    
end


function pair_feat=do_calc_pair_feat_triplet(hfeat, relation_map)
       

    e_hfeat=hfeat(relation_map(:,1),:);
    en_hfeat=hfeat(relation_map(:,2),:);
    ep_hfeat=hfeat(relation_map(:,3),:);
            
    pair_feat=calc_hfeat_dist(e_hfeat,en_hfeat)-calc_hfeat_dist(e_hfeat,ep_hfeat);

end


function pair_feat=do_calc_pair_feat_duplet(hfeat, relation_map, relevant_sel)
       

    e_hfeat=hfeat(relation_map(:,1),:);
    right_hfeat=hfeat(relation_map(:,2),:);
        
  
    pair_feat=calc_hfeat_dist(e_hfeat,right_hfeat);
    
    
    assert(isnumeric(pair_feat));
    pair_feat(relevant_sel,:)=-pair_feat(relevant_sel,:);

end
