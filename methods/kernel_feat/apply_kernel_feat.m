

function feat_data=apply_kernel_feat(wl_model, feat_data)
   
   
    % should not use sparse value, it would be very slow.
    assert(~issparse(feat_data));
    
   
    support_vectors=wl_model.support_vectors;

    e_num = size(feat_data,1);
    feat_data_kernel = sqdist(feat_data',support_vectors');
    feat_data_kernel = exp(-feat_data_kernel/(2*wl_model.sigma));
    feat_data_kernel = feat_data_kernel-repmat(wl_model.feat_data_mean,e_num,1);

    feat_data=feat_data_kernel;


end