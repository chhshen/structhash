



function train_result=sh_train(train_info, train_data)


fprintf('\n\n------------------------------structh_hash_stage_train ----------------------------------------\n\n');


if isfield(train_info, 'wl_cyclic_iter_num')
    train_info.max_iteration_num=train_info.bit_num*train_info.wl_cyclic_iter_num;
else
    train_info.max_iteration_num=train_info.bit_num;
end
train_info.train_id=train_info.method_id;
train_info.valid_wl_num=train_info.bit_num;
    


train_data.feat_data=full(double(train_data.feat_data));




train_info.train_data=train_data;
train_info=do_boost_learn_config(train_info);
train_result=train_info.boost_learn_fn(train_info);

train_result.objfv_eva_iters=1:train_info.max_iteration_num;
train_result.trn_time_iters=train_result.method_time_iters;


fprintf('\n---------- train_result: ------------\n\n');
disp(train_result);        
    
end



function [train_info]=do_boost_learn_config(train_info)

train_info=boost_learn_config_sh_stage(train_info);

end

