

function relation_info=gen_training_ranking_info(train_data, relation_config)


fprintf('\n\n----------------------------- gen_training_ranking_info ----------------------------\n\n');



relevant_knn_num=relation_config.relevant_knn_num;
irrelevant_knn_num=relation_config.irrelevant_knn_num;

top_knn_sample_type='random';


relation_info=do_gen_relation_info(train_data, relevant_knn_num, irrelevant_knn_num, top_knn_sample_type, relation_config);
relation_info.ds_e_inds=train_data.ds_e_inds;

      
      
end






function relation_info=do_gen_relation_info(train_data, top_k, top_k_neg, top_knn_sample_type, relation_config)


assert(strcmp(top_knn_sample_type,'random'));


e_num=train_data.e_num;


if ~isfield(relation_config, 'self_relevant');
    relation_config.self_relevant=false;
end




  
relevant_infos=cell(e_num,1);
% relevant_num_es=zeros(e_num,1);

irrelevant_infos=cell(e_num,1);


shared_task_data=[];
shared_task_data.top_k=top_k;
shared_task_data.top_k_neg=top_k_neg;
shared_task_data.relation_config=relation_config;
shared_task_data.label_info=train_data.label_info;


disp_step=0.05;
disp_thresh=disp_step;

for e_idx=1:e_num

	task_input=e_idx;
	task_result=do_gen_relation_one_task(task_input, shared_task_data);
	one_relevant_info=task_result.one_relevant_info;
	one_irrelevant_info=task_result.one_irrelevant_info;
    

	relevant_infos{e_idx}=one_relevant_info;
	irrelevant_infos{e_idx}=one_irrelevant_info;
    

    finish_rate=e_idx/e_num;
    if finish_rate>=disp_thresh
        fprintf(' %.2f ', finish_rate);
        disp_thresh=disp_thresh+disp_step;
    end
        
end



fprintf('<--done!\n');


relation_info=[];
relation_info.relevant_infos=relevant_infos;
relation_info.irrelevant_infos=irrelevant_infos;

% relation_info.relevant_num_es=relevant_num_es;
% relation_info.irrelevant_num_es=irrelevant_num_es;

relation_info=do_gen_rel_mat(relation_info, relation_config);


end








function task_result=do_gen_relation_one_task(task_input, shared_task_data)

	one_relevant_info=[];
	one_irrelevant_info=[];

	top_k=shared_task_data.top_k;
	top_k_neg=shared_task_data.top_k_neg;
	relation_config=shared_task_data.relation_config;
	label_info=shared_task_data.label_info;	


	e_idx=task_input;

    [one_rel_sel, one_irrel_sel]=gen_one_relevant_info(label_info, e_idx, relation_config);
    
    relevant_idxes_ds=find(one_rel_sel);
    
    if ~isempty(relevant_idxes_ds)
        rel_num_ds=length(relevant_idxes_ds);

        relevant_idxes_ds=uint32(relevant_idxes_ds);

        if rel_num_ds> top_k


            sel_idxes=randsample(rel_num_ds, top_k);

            % Warning: the relevant idxes is not in order....
            % thus the value of relevant_infos and get_rel_idxes is not the same....

            one_relevant_info=relevant_idxes_ds(sel_idxes);

    %         relevant_num_es(e_idx)=length(relevant_infos{e_idx});

        else

            one_relevant_info=relevant_idxes_ds;
    %         relevant_num_es(e_idx)=rel_num_ds;

        end
    end
    
    
    irelevant_idxes_ds=find(one_irrel_sel);

    if ~isempty(irelevant_idxes_ds)
        irrel_num_ds=length(irelevant_idxes_ds);

        irelevant_idxes_ds=uint32(irelevant_idxes_ds);

        if irrel_num_ds> top_k_neg
            sel_idxes=randsample(irrel_num_ds, top_k_neg);
            one_irrelevant_info=irelevant_idxes_ds(sel_idxes);
%             irrelevant_num_es(e_idx)=top_k_neg;
        else
            one_irrelevant_info=irelevant_idxes_ds;
%             irrelevant_num_es(e_idx)=irrel_num_ds;
        end
     end
    

    task_result=[];
    task_result.one_relevant_info=one_relevant_info;
    task_result.one_irrelevant_info=one_irrelevant_info;
        
    
        
end





function relation_info=do_gen_rel_mat(relation_info, relation_config)


fprintf('\n ------------- gen_training_ranking_info: gen_rel_mat \n')

if isfield(relation_info, 'rel_mat')
    rel_mat=relation_info.rel_mat;
else
    relevant_infos=relation_info.relevant_infos;
    rel_mat=gen_rel_mat(relevant_infos, relation_config);
end

fprintf('\n ------------- gen_training_ranking_info: gen_irrel_mat \n')

if isfield(relation_info, 'irrel_mat')
    irrel_mat=relation_info.irrel_mat;
else
    irrel_mat=[];
    irrelevant_infos=relation_info.irrelevant_infos;
    if ~isempty(irrelevant_infos)
        irrel_mat=gen_rel_mat(irrelevant_infos, relation_config);
    end
end

% not support this...
assert(~isempty(irrel_mat));


% relation_info.relevant_infos=[];
% relation_info.irelevant_infos=[];


relation_info.rel_mat=rel_mat;
relation_info.irrel_mat=irrel_mat;
relation_info.e_num=size(rel_mat,1);
relation_info.r_num_symm=nnz(rel_mat)+nnz(irrel_mat);



relation_info.get_rel_idxes_fn=@get_rel_idxes;
relation_info.get_irrel_idxes_fn=@get_irrel_idxes;



end










function rel_idxes=get_rel_idxes(relation_info, e_idx)

one_rel=relation_info.rel_mat(:, e_idx);
rel_idxes=find(one_rel);

end


function irrel_idxes=get_irrel_idxes(relation_info, e_idx)

irrel_mat=relation_info.irrel_mat;
if isempty(irrel_mat)
    one_rel=relation_info.rel_mat(:, e_idx);
    one_rel(e_idx)=false;
    irrel_idxes=find(~one_rel);
else
    irrel_idxes=find(irrel_mat(:, e_idx));
end

end





function rel_mat=gen_rel_mat(relevant_infos, relation_config)


e_num=length(relevant_infos);
% relation_map_pos=cell(e_num,1);
rel_mat=cell(1, e_num);


disp_step=0.05;
disp_thresh=disp_step;


for e_idx=1:e_num
          
        
    p_top_knn_inds=relevant_infos{e_idx};
    
    % exclude self, assert this is true...
%     p_top_knn_inds=p_top_knn_inds(p_top_knn_inds~=e_idx);

%     one_relation_map_col=p_top_knn_inds;
%     one_r_num=length(one_relation_map_col);
%     one_relation_map=zeros(one_r_num, 2);
%     one_relation_map(:, 2)=one_relation_map_col;
%     one_relation_map(:, 1)=e_idx;
%     relation_map_pos{e_idx}=one_relation_map;

    p_num=length(p_top_knn_inds);
    rel_mat{e_idx}=sparse(double(p_top_knn_inds), ones(p_num,1), true(p_num,1), e_num, 1);
    
    finish_rate=e_idx/e_num;
    if finish_rate>=disp_thresh
        fprintf(' %.2f ', finish_rate);
        disp_thresh=disp_thresh+disp_step;
    end

end

fprintf(' <--done!\n');




rel_mat=cell2mat(rel_mat);


assert(islogical(rel_mat));
rel_mat=rel_mat|rel_mat';


end

















function [rel_sel irrel_sel]=gen_one_relevant_info(label_info, e_idx, relation_config)


rel_sel=[];
irrel_sel=[];

org_rel_sel=[];

label_type=label_info.label_type;

if strcmp(label_type, 'multiclass');
    y=label_info.label_data;
    e_num=length(y);
    assert(e_num<2^32);

    rel_sel=y==y(e_idx);
    
    org_rel_sel=rel_sel;
end



if strcmp(label_type, 'multilabel');
    
    y=label_info.label_data;
    one_labels_sel=y(e_idx,:);
%     one_labels_sel=logical(one_labels_sel);
    tmp_sel_y=y(:, one_labels_sel);
        
%     rel_sel=any(tmp_sel_y, 2);
    
    
    similarity_thresh=relation_config.mlabel_sim_thresh;
    rel_sel=sum(tmp_sel_y, 2)>=similarity_thresh;
    
    org_rel_sel=rel_sel;
end


if strcmp(label_type, 'relmap');
    
    relevant_map=label_info.knn_info.relevant_map;
        
    e_num=size(relevant_map,1);
    assert(e_num<2^32);
    
    rel_sel=relevant_map(:, e_idx)|relevant_map(e_idx, :)';
%     rel_sel=relevant_map(:, e_idx);
    
    org_rel_sel=rel_sel;
end


if strcmp(label_type, 'relation');
    
    relation_info=label_info.relation_info;
    e_num=length(relation_info.relevant_infos);
    rel_sel=false(e_num, 1);
    rel_sel(relation_info.relevant_infos{e_idx})=true;

    if isfield(relation_info, 'irrelevant_infos')
        irrel_sel=false(e_num, 1);
        irrel_sel(relation_info.irrelevant_infos{e_idx})=true;
    end
    
    org_rel_sel=rel_sel;
end



assert(~isempty(rel_sel));
rel_sel=full(rel_sel);


if isempty(irrel_sel)

    assert(~isempty(org_rel_sel));
    org_rel_sel=full(org_rel_sel);
    
    irrel_sel=~org_rel_sel;

       % TO verify:
%     if nnz(rel_sel)>0
%         irrel_sel=~rel_sel;
%     else
%         % this means this item is not valid, it is relation only can be determined by other examples.
%         % cuz the relation will be make symmetric later
%         irrel_sel=false(size(rel_sel));
%     end

end


% all other caculation depends on ths....
assert(~relation_config.self_relevant);

%not including itself!!!
if ~relation_config.self_relevant
    rel_sel(e_idx)=false;
end




end




