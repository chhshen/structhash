


% Code author: Guosheng Lin and Fayao Liu 
% Contact: guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au, fayaoliu@gmail.com


function demo_structhash


%mosek path, change to your own mosek path
mosek_path='./libs/mosek/7/toolbox/r2013aom';
addpath(mosek_path);

if ~exist(mosek_path, 'dir')
	error('MOSEK 7.0 not found!!!');
end


addpath('./libs/lbfgsb-for-matlab');
addpath('./libs/mlr');
addpath('./libs');
addpath('./evaluate');
addpath(genpath([pwd '/methods']));



thread_num=4;
unix_cmd=sprintf('export OMP_NUM_THREADS=%d', thread_num);
unix(unix_cmd, '-echo');






% try larger bits: 32, 64, ...
bit_num=16;


% method running flags:
% in the default setting, 3 methods will be run: 
% StuctH-stage-SNDCG, StuctH-stage-SNDCG-kernel and LSH.


run_StructH=true;
% run_StructH=false;

run_StructH_Kernel=true;
% run_StructH_Kernel=false;

run_LSH=true;
% run_LSH=false;


%--------------------------------------------------------------------------------------------------
% load data



% try this dataset:

ds_file='./datasets/MNIST.mat';
ds_name='MNIST';


% load the demo dataset:
ds=load(ds_file);
ds=ds.dataset;


% here is a very simple way to split training data, just for demo

disp('Randomly generating data training and test splits...')
disp('Note that the splits here are just for demo, which are not the same as that in the paper.');    



e_num=length(ds.y);
tst_e_num=min(2000, round(e_num*0.3));
tst_sel=false(e_num,1);
tst_sel(randsample(e_num,tst_e_num))=true;
ds.test_inds=find(tst_sel);

database_sel=~tst_sel;
ds.db_inds=find(database_sel);

db_e_num=length(ds.db_inds);
trn_e_num=min(2000, db_e_num);
ds.train_inds=ds.db_inds(randsample(db_e_num,trn_e_num));


database_data=[];
database_data.feat_data=ds.x(ds.db_inds,:);
database_data.label_data=ds.y(ds.db_inds);

train_data=[];
train_data.feat_data=ds.x(ds.train_inds,:);
train_data.label_data=ds.y(ds.train_inds);

test_data=[];
test_data.feat_data=ds.x(ds.test_inds,:);
test_data.label_data=ds.y(ds.test_inds);



% do normalization.

max_dims=max(train_data.feat_data,[],1);
min_dims=min(train_data.feat_data,[],1);
range_dims=max_dims-min_dims+eps;


database_data.feat_data = bsxfun(@minus, database_data.feat_data, min_dims);
database_data.feat_data = bsxfun(@rdivide, database_data.feat_data, range_dims);

train_data.feat_data = bsxfun(@minus, train_data.feat_data, min_dims);
train_data.feat_data = bsxfun(@rdivide, train_data.feat_data, range_dims);

test_data.feat_data = bsxfun(@minus, test_data.feat_data, min_dims);
test_data.feat_data = bsxfun(@rdivide, test_data.feat_data, range_dims);



% we also support other label type, like multilabel, or ranking, 
% please refer to gen_training_ranking_info for details

label_type='multiclass';
train_data.label_type=label_type;


%--------------------------------------------------------------------------------------------------
% evaluate methods:


db_label_info.label_data=database_data.label_data;
db_label_info.label_type=label_type;

test_label_info.label_data=test_data.label_data;
test_label_info.label_type=label_type;

eva_data_info=[];
eva_data_info.db_label_info=db_label_info;
eva_data_info.test_label_info=test_label_info;


eva_bit_step=ceil(min(8, bit_num/4));
eva_bits=eva_bit_step:eva_bit_step:bit_num;


eva_param=[];
eva_param.eva_top_knn_pk=100;
eva_param.eva_top_knn_ndcg=100;
eva_param.eva_top_knn_map=1e5;

predict_results=cell(0);




%-----------------------------------------------


run_config=[];
run_config.bit_num=bit_num;
run_config.eva_bits=eva_bits;
run_config.eva_param=eva_param;
run_config.eva_data_info=eva_data_info;
run_config.ds_name=ds_name;





%-----------------------------------------------
% run StructH    

% please refer to the function: do_config_sh to set the parameters for StructH
    
if run_StructH
        
	sh_run_result=do_run_StructH(run_config, database_data, train_data, test_data);
	predict_results{end+1}=sh_run_result.predict_result;

end


%--------------------------------------------------------------------------------------------------
% run StructH-Kernel


if run_StructH_Kernel

	shk_run_result=do_run_StructH_Kernel(run_config, database_data, train_data, test_data);
	predict_results{end+1}=shk_run_result.predict_result;
    
end




%--------------------------------------------------------------------------------------------------
% run LSH for a simple comparison:

if run_LSH

	lsh_run_result=do_run_LSH(run_config, database_data, train_data, test_data);
	predict_results{end+1}=lsh_run_result.predict_result;
    
end



do_show_result(predict_results, run_config);




end










function train_info=do_config_sh(run_config, train_data)


    label_info=[];
    label_info.label_type=train_data.label_type;
    label_info.label_data=train_data.label_data;
    train_data.label_info=label_info;

    train_data.e_num=size(train_data.feat_data, 1);
    train_data.ds_e_inds=uint32(1:train_data.e_num)';


    % here is a simple example for generating triplets on multi-class dataset.
    % user should implement their own way for generating triplets according to applications or datasets.

    % generally the large value of knn_num may achieve better results, but it will be slower for large value.
    relation_config=[];
    relation_config.relevant_knn_num=50;
    relation_config.irrelevant_knn_num=100;


    % this is an example for generating training rankings, 
    % user should have their own implementation on generating training ranking according to the applications or datasets.
    relation_info=gen_training_ranking_info(train_data, relation_config);

    % NOTES:  relation_info should be cached to avoid repeating generation of ranking information







	% SructH configuration:
	train_info=[];

	% the parameter setting of C, using a large value might lead to better
    % performance but the training will be slower
	train_info.tradeoff_C=10^6;
%     train_info.tradeoff_C=10^7;
    
    % cutting-plane stop criteria, using a small value might lead to better
    % performance but the training will be slower
    train_info.cp_epsilon=0.001;


    % a setting for faster training, but the ranking performance will be reduced:
    % train_info.tradeoff_C=10^5;
%     train_info.cp_epsilon=0.01;



	train_info.bit_num=run_config.bit_num;
	train_info.relation_info=relation_info;


	train_info.method_name_subfix=[];
        
        
    train_info.precision_at_k=inf;
    train_info.label_loss_types=[];
    

    % optimize the efficient simplified NDCG loss
    train_info.label_loss_type='SNDCG';

    % optimize the original NDCG loss
%     train_info.label_loss_type='NDCG';

    % optimize the AUC loss
%     train_info.label_loss_type='AUC';
      

    % efficient stage-wise training of structH
    use_stage_train=true;  

    % uncomment the following one to turn on original training of structH
%     use_stage_train=false;  

      
     
     train_info.use_stage_train=use_stage_train;
        
    
    if use_stage_train

    	train_info.method_id='sh_stage';
    	train_info.method_name_disp='StructH-stage';
        train_info.sh_train_fn=@sh_train_stage;
                
        train_info.solver_type='cutting-plane-stage';
        
        train_info.use_weight_hamming=false;
               	    
    else
        
        train_info.method_id='sh_original';
        train_info.method_name_disp='StructH-original';
        train_info.sh_train_fn=@sh_train_original;
                
        train_info.solver_type='cutting-plane-original';
        	
        train_info.use_weight_hamming=true;
    	               
    end
    
    train_info.method_id=[train_info.method_id '_' train_info.label_loss_type];
    train_info.method_name_disp=[train_info.method_name_disp '-' train_info.label_loss_type];
    
    train_info.cp_hot_start=true;
    train_info.method_name=train_info.method_id;


% --------------------------------------------------------------
	% hash learner configure

	train_info.do_wl_train_spectral=true;
	train_info.do_wl_train_random=true;
	train_info.do_wl_train_lbfgs=true;
	train_info.do_wl_train_stump=false;


	if train_info.do_wl_train_random
	    train_info.wlearner_random_sample_num=100;
	end

	if train_info.do_wl_train_lbfgs
	    train_info.lb_epsilon=1e-4;
	    train_info.max_lbfgs_iter=50;
	end


	train_info.max_valid_wlearner_r_num=inf;
	train_info.max_relation_num_per_e=inf;


% --------------------------------------------------------------
% cutting-plane config


train_info.norm_type=1;

% margin rescale
train_info.rescale_type=2;    
% slack rescale
% train_info.rescale_type=1;  

% only support margin rescale currently
assert(train_info.rescale_type==2);





    
label_loss_type=train_info.label_loss_type;
do_infer_fn_mlr=[];
infer_fn=[];
    
if strcmp(label_loss_type, 'AUC')
    infer_fn=@my_infer_auc;
end

if strcmp(label_loss_type, 'SNDCG')
    infer_fn=@my_infer_sndcg;
end

if strcmp(label_loss_type, 'NDCG')
    infer_fn=@my_infer_mlr;
    do_infer_fn_mlr=@my_separationOracleNDCG;
end
    

train_info.infer_fn=infer_fn;
train_info.do_infer_fn_mlr=do_infer_fn_mlr;
    


end




function run_result=do_run_StructH(run_config, database_data, train_data, test_data)



	
	train_info=do_config_sh(run_config, train_data);	    
    
    train_info.use_wl_kernel=false;

	sh_train_result=train_info.sh_train_fn(train_info, train_data);
    sh_model=sh_train_result.model;
    
    
    fprintf('\n---------------------evaluating hashing method:%s---------------------\n', train_info.method_name_disp);
    

    db_data_code=sh_encode(sh_model, database_data.feat_data);
    tst_data_code=sh_encode(sh_model, test_data.feat_data);


    % generate compact code if it is needed:
    % sh_db_data_code_compact=gen_compactbit(sh_db_data_code);

    
    eva_bits=run_config.eva_bits;
    eva_data_info=run_config.eva_data_info;
    eva_param=run_config.eva_param;
    
        
    if train_info.use_weight_hamming
        eva_param.use_weight_hamming=true;
        eva_param.hamming_weight=sh_model.w;
    end
    
    
    predict_result=do_eva(eva_bits, db_data_code, tst_data_code, eva_data_info, eva_param);
    predict_result.method_name=train_info.method_name_disp;
    disp(predict_result);
       

    run_result=[];
    run_result.predict_result=predict_result;
    
end











function run_result=do_run_StructH_Kernel(run_config, database_data, train_data, test_data)


	train_info=do_config_sh(run_config, train_data);    
    
    train_info.use_wl_kernel=true;

	train_info.method_name_disp=[train_info.method_name_disp '-Kernel'];
    train_info.method_id=[train_info.method_id '_kernel'];


    % using RBF kernel

    % this is a simple example for picking the RBF kernel parameter sigma
    % user should tune this parameter, e.g., try to pick v from [0.1 1 5 10].

    trn_feat_data=train_data.feat_data;
    nn_k=50;
    if size(trn_feat_data,1)>1e4
        trn_feat_data=trn_feat_data(randsample(size(trn_feat_data,1), 1e4),:);
    end
    sq_eudist = sqdist(trn_feat_data',trn_feat_data');
    sq_eudist=sort(sq_eudist,2);
    sq_eudist=sq_eudist(:,1:nn_k);
    sq_sigma = mean(sq_eudist(:));
    sq_sigma=sq_sigma+eps;

    % this need to tune, try to pick v from [0.1 1 5 10]
    v=1;
    train_info.sigma=sq_sigma*v;


    % random select the support vectors, as an alternative, user can use
    % k-means for setting this support_vectors.
    sv_num=500;
    sv_num=min(sv_num, size(trn_feat_data, 1));
    support_vectors=trn_feat_data(randsample(size(trn_feat_data, 1), sv_num),:);
    train_info.support_vectors=support_vectors;

    
    assert(~issparse(train_data.feat_data));
    [train_data.feat_data, wl_kernel_model]=gen_kernel_feat(train_info, train_data.feat_data);
        
    shk_train_result=train_info.sh_train_fn(train_info, train_data);
%     shk_train_result.model.wl_kernel_model=wl_kernel_model;


    fprintf('\n---------------------evaluating hashing method:%s---------------------\n', train_info.method_name_disp);
    
    shk_model=shk_train_result.model;

    database_data.feat_data=wl_kernel_model.apply_fn(wl_kernel_model, database_data.feat_data);
    db_data_code=sh_encode(shk_model, database_data.feat_data);
    
    test_data.feat_data=wl_kernel_model.apply_fn(wl_kernel_model, test_data.feat_data);
    tst_data_code=sh_encode(shk_model, test_data.feat_data);

    eva_bits=run_config.eva_bits;
    eva_data_info=run_config.eva_data_info;
    eva_param=run_config.eva_param;
    
    if train_info.use_weight_hamming
        eva_param.use_weight_hamming=true;
        eva_param.hamming_weight=shk_model.w;
    end

    predict_result=do_eva(eva_bits, db_data_code, tst_data_code, eva_data_info, eva_param);
    predict_result.method_name=train_info.method_name_disp;
    disp(predict_result);
    
    run_result=[];
    run_result.predict_result=predict_result;


end












function run_result=do_run_LSH(run_config, database_data, train_data, test_data)

    bit_num=run_config.bit_num;
    eva_bits=run_config.eva_bits;
    eva_data_info=run_config.eva_data_info;
    eva_param=run_config.eva_param;
    
    fprintf('\n---------------------evaluating hashing method:%s---------------------\n', 'LSH');

	lsh_model=LSH_learn(train_data.feat_data, bit_num);
    db_data_code=LSH_compress(database_data.feat_data, lsh_model);
    tst_data_code=LSH_compress(test_data.feat_data, lsh_model);

    
    predict_result=do_eva(eva_bits, db_data_code, tst_data_code, eva_data_info, eva_param);
    predict_result.method_name='LSH';
    disp(predict_result);
       

    run_result=[];
    run_result.predict_result=predict_result;


end





function predict_result=do_eva(eva_bits, db_data_code, tst_data_code, eva_data_info, eva_param)


    predict_result=[];
    predict_result.eva_bits=eva_bits;
    for b_idx=1:length(eva_bits)
        one_bit_num=eva_bits(b_idx);
        lsh_eva_data_info=eva_data_info;
        lsh_eva_data_info.db_data_code=db_data_code(:, 1:one_bit_num);
        lsh_eva_data_info.tst_data_code=tst_data_code(:, 1:one_bit_num);
        one_bit_result=hash_evaluate(eva_param, lsh_eva_data_info);
        predict_result.pk100_eva_bits(b_idx)=one_bit_result.pk100;
        predict_result.ndcg100_eva_bits(b_idx)=one_bit_result.ndcg100;
        predict_result.map_eva_bits(b_idx)=one_bit_result.map;
    end

end






function do_show_result(predict_results, run_config)


for p_idx=1:length(predict_results)
    
    predict_result=predict_results{p_idx};
   
    fprintf('\n\n-------------predict_result--------------------------------------------------------\n\n');
    disp(predict_result);

end



do_show_result_one_eva(predict_results, run_config, 'pk100', 'Precision @ K (K=100)');
do_show_result_one_eva(predict_results, run_config, 'map', 'mAP');
do_show_result_one_eva(predict_results, run_config, 'ndcg100', 'NDCG (100-NN)');



end








function do_show_result_one_eva(predict_results, run_config, eva_name, eva_name_disp)


%-----------------------------------------------

eva_bits=run_config.eva_bits;
ds_name=run_config.ds_name;


f1=figure;


defual_unit=get(f1,'units');
set(f1,'units','normalized');
screenpos = get(gcf,'Position');
screenpos(1)=rand(1).*0.5;
screenpos(2)=rand(1).*0.5;
set(f1,'position',screenpos);
set(f1,'units',defual_unit);


line_width=2;
xy_font_size=22;
marker_size=10;
legend_font_size=15;
xy_v_font_size=15;
title_font_size=xy_font_size;

legend_strs=cell(length(predict_results), 1);

for p_idx=1:length(predict_results)
    
    predict_result=predict_results{p_idx};
       
    color=gen_color(p_idx);
    marker=gen_marker(p_idx);
    
    x_values=eva_bits;
    y_values=predict_result.([eva_name '_eva_bits']);

    p=plot(x_values, y_values);
    
    set(p,'Color', color)
    set(p,'Marker',marker);
    set(p,'LineWidth',line_width);
    set(p,'MarkerSize',marker_size);
    
    legend_strs{p_idx}=[predict_result.method_name  sprintf('(%.3f)', y_values(end))];
    
    hold all
end


hleg=legend(legend_strs);
h1=xlabel('Number of bits');
h2=ylabel(eva_name_disp);
title(ds_name, 'FontSize', title_font_size);

set(gca,'XTick',x_values);
xlim([x_values(1) x_values(end)]);
set(hleg, 'FontSize',legend_font_size);
set(hleg,'Location','SouthEast');
set(h1, 'FontSize',xy_font_size);
set(h2, 'FontSize',xy_font_size);
set(gca, 'FontSize',xy_v_font_size);
set(hleg, 'FontSize',legend_font_size);
grid on;
hold off


end




