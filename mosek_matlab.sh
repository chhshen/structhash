

MOSEKDIR=./libs/mosek
MOSEKPLATFORM=linux64x86
export PATH=$PATH:$MOSEKDIR/7/tools/platform/$MOSEKPLATFORM/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MOSEKDIR/7/tools/platform/$MOSEKPLATFORM/bin
export MOSEKLM_LICENSE_FILE=$MOSEKDIR/7/licenses/mosek.lic

mosek -f

# matlab -nodisplay
matlab
