# Structured Learning of Binary Code with Column Generation for Optimizing Ranking Measures

__This is the implementation of the following paper. If you use this code in your research,
please cite our paper__

```
 @inproceedings{lin2014optimizing,
  title={Optimizing Ranking Measures for Compact Binary Code Learning},
  author={Lin, Guosheng and Shen, Chunhua and Wu, Jianxin},
  booktitle={European Conference on Computer Vision},
  pages={613--627},
  year={2014},
}

```

This code also includes the extensions of StructHash on efficient stage-wise learning 
and using a simplified NDCG loss for efficient ranking inference.


## Install

The following libraries/toolboxes are required for running our code:

Require MOSEK 7.0 <https://www.mosek.com/>. Please follow their instructions for installation.

The LBFGS-B solver is used our implementation. A MATLAB wrapper of LBFGS-B is included here (pre-compiled in Linux, OSX),
    which can be downloaded in: <https://github.com/pcarbo/lbfgsb-matlab>. This library is placed in the folder ./libs

The NDCG inference implementation is from the code for Metric Learning to Rank in: <https://github.com/bmcfee/mlr>.
This library is placed in the folder ./libs


## Running

The code is  tested under Ubuntu 14.04.

Edit the file demo_structhash.m to change the MOSEK toolbox location.

Edit the script file mosek_matlab.sh for MOSEK environment setup.

Run mosek_matlab.sh to start MATLAB.

Run the demo file demo_structhash.m which shows how to use the code.




## People

Guosheng Lin, Fayao Liu, Chunhua Shen.

Code authors: Guosheng Lin and Fayao Liu.
This code is provided for non-profit research purpose only; and is released under the GNU license. 
For commercial applications, please contact Chunhua Shen http://www.cs.adelaide.edu.au/~chhshen/.

